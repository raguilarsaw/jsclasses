
//Session Storage Get ContactList cuando si hay contactos
var contactList = [];
if(window.sessionStorage.getItem('contactList') != null && window.sessionStorage.getItem('contactList') != '[]'){
 contactList = JSON.parse(window.sessionStorage.getItem('contactList'));
} 

//CLASSES
var ContactOperation = function(){
};

//Actualizar o Limpiar el Session Storage, hay que pasarlo a string
ContactOperation.prototype.uptadeStorage = function() {
	window.sessionStorage.setItem('contactList', JSON.stringify(contactList));
}

//Crear un Nuevo Contacto
ContactOperation.prototype.newContact = function(contact_id,name,last_name,phone,home_phone,mail,address){
	var contact= {
		id:contact_id,
		name : name,
		last_name:last_name,
		phone : phone,
		home_phone:home_phone,
		mail : mail,
		address:address
	}; 
  contactList.push(contact);
  Contact.uptadeStorage();
};


//Borrar Contacto
ContactOperation.prototype.deleteContact = function(contact_id){
	for(var i=0;i<contactList.length;i++){
		if(contactList[i].id == contact_id){
			contactList.splice(i,1);
		}
	}
	Contact.uptadeStorage();
};

//Borrar Todos los Contactos
ContactOperation.prototype.deleteAllContact = function(contact_id){
	contactList=[];	
	Contact.uptadeStorage();
};

//Editar Contacto
ContactOperation.prototype.editContact = function(contact_id,name,last_name,phone,home_phone,mail,address){
	for(var i=0;i<contactList.length;i++){
		if(contactList[i].id == contact_id){
			var _this = contactList[i];
			_this.name = name;
			_this.last_name = last_name;
			_this.phone = phone;
			_this.home_phone = home_phone;
			_this.mail = mail;
			_this.address = address;
		}
	}
	Contact.uptadeStorage();
};

//Para crear un id basado en la fecha/tiempo en que se creo
ContactOperation.prototype.getContactId = function(){
	var _date = new Date();
	return _date.getTime();
};

var Contact = new ContactOperation();



//DOM fUNCTIONS

//Trae la List de Contactos, si tiene
function getContactList(){
	$('#contact-parent').html('');
	cleanForm();
	if(contactList.length == 0){
		$('.nocontacts-popup').show();
	}else{
		$('.nocontacts-popup').hide();

		for(var i=0;i<contactList.length;i++){
			$('#contact-parent').append('<div class="contact-label" onclick="getContactInfo(' + contactList[i].id +')">	<div class="contact-name">'+contactList[i].name+' '+contactList[i].last_name+'</div> <div class="phone-number">'+contactList[i].phone+'</div></div>');
		}
	}
}

//Muestra la informacion del Contacto cuando se Selecciona
function getContactInfo(contact_id){
	$('.save-button').hide();

	for(var i=0;i<contactList.length;i++){
		if(contactList[i].id == contact_id){
			$('.DeleteContact-btn, .edit-save').data("contact_id_number", contact_id);// setear id a los botones de edit y delete
			$('.contact-FirstName').val(contactList[i].name); // llenar el form
  			$('.contact-LastName').val(contactList[i].last_name);
  			$('.contact-Phone1').val(contactList[i].phone);
  			$('.contact-Phone2').val(contactList[i].home_phone);
  			$('.contact-Email').val(contactList[i].mail);
  			$('.contact-Address').val(contactList[i].address);
  			$('#contact-form input').prop('disabled', true); // inhabilitar todos los input 
  			$('#contact-form').show();// mostrar form
		}
	}
}

// Limpiar el Form, despues de borrar un contacto o al iniciar la function
function cleanForm(){
	$('.DeleteContact-btn, .edit-save').data("contact_id_number",'');
	$('#contact-form, .save-button').hide();
	$('.contact-FirstName, .contact-LastName, .contact-Phone1, .contact-Phone2, .contact-Email, .contact-Address').val('');
}


$(function(){
	getContactList();
	//Para eliminar un contacto
	$('.DeleteContact-btn').click(function(){
		var contact_id = $(this).data('contact_id_number');
		if(contact_id == ''){
			return false;
		}
		if(confirm("Delete Contact?")){
			Contact.deleteContact(contact_id);
			getContactList();
		}
	});


	//Para eliminar un contacto
	$('.options-btn').click(function(){
		if(confirm("Delete all Contacts?")){
			Contact.deleteAllContact();
			getContactList();
		}
	});


	//Para mostar el boton de edit
	$('.EditContact-btn').click(function(){
		$('#contact-form input').prop('disabled', false); //habilitar input
		$('.new-save').hide();
		$('.edit-save').show();
	});


	//Para mostar el boton de add new
	$('.AddContact-btn').click(function(){
		cleanForm();
		$('#contact-form input').prop('disabled', false); //habilitar input
		$('#contact-form, .new-save').show();
	});

	//Boton de Salvar cuando es contacto nuevo
	$('.new-save').click(function(){
		var contact_id = Contact.getContactId(),
		    name = $('.contact-FirstName').val(),
	  		last_name = $('.contact-LastName').val(),
		  	phone = $('.contact-Phone1').val(),
		  	home_phone = $('.contact-Phone2').val(),
		  	mail = $('.contact-Email').val();
		  	address = $('.contact-Address').val();
		Contact.newContact(contact_id,name,last_name,phone,home_phone,mail,address);
		getContactList();
	});

	//Boton de Salvar cuando edita
	$('.edit-save').click(function(){
		var contact_id = $(this).data('contact_id_number');
		if(contact_id == ''){
			return false;
		}
		var name = $('.contact-FirstName').val(),
	  		last_name = $('.contact-LastName').val(),
		  	phone = $('.contact-Phone1').val(),
		  	home_phone = $('.contact-Phone2').val(),
		  	mail = $('.contact-Email').val();
		  	address = $('.contact-Address').val();
		Contact.editContact(contact_id,name,last_name,phone,home_phone,mail,address);
		getContactList();
	});


});
